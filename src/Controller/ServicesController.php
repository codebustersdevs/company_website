<?php
    namespace App\Controller;

    use Symfony\Bundle\FrameworkBundle\Controller\Controller;

    /**
     *
     */
    class ServicesController extends Controller
    {

        function index()
        {
            return $this->render( 'services_page.html.twig' );
        }
    }

?>
