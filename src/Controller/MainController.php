<?php
    namespace App\Controller;

    use Symfony\Bundle\FrameworkBundle\Controller\Controller;

    /**
     *
     */
    class MainController extends Controller
    {

        function index()
        {
            return $this->render( 'home_page.html.twig' );
        }
    }

?>
