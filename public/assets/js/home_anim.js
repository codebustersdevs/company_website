jQuery(window).load( function(){
    jQuery('#slogan').animate({opacity: 1}, 800);
    jQuery('#header').animate({opacity: 1}, 800);
    jQuery('.home-bg').delay(500).animate({opacity: 1}, 800);

    jQuery(window).scroll( function(){
        var reveal_offset = 100;
        var win_bottom = jQuery(window).scrollTop() + jQuery(window).height();
        var vis_h_top = jQuery('#vision-header').offset().top + reveal_offset;
        if( win_bottom > vis_h_top){
            jQuery('#vision-header').animate({opacity: 1}, 1000, 'linear');
        }

        jQuery('.vision-par').each( function(i){
            var vis_p_top = jQuery(this).offset().top + jQuery(this).outerHeight();
            if( win_bottom > vis_p_top ){
                jQuery(this).animate({opacity: 1}, 1000);
            }
        });
    });
});
