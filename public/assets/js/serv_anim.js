jQuery(window).load( function(){
    //initial animations
    jQuery('#serv-title').animate({opacity: 1}, 600);
    jQuery('#header').animate({opacity: 1}, 600);
    jQuery('hr').animate({opacity: 1}, 600);
    jQuery('.serv-icon-row').delay(500).animate({opacity: 1}, 600);
    jQuery('.services-bg').delay(800).animate({opacity: 1}, 600);
    jQuery('#desc-container').delay(1000).animate({opacity: 1}, 600);

    //initialize desc state
    jQuery('#desc-container').css('height', jQuery('.service-desc:first').outerHeight());
    jQuery('#app-desc').addClass('show-desc');

    //desc icon click actions
    jQuery('.service-icon').click(function(i){
        var splt_arr = jQuery(this).attr('id').split('-');
        var desc_id = splt_arr[0] + '-desc';
        jQuery('.service-desc').removeClass('show-desc');
        jQuery('#'+desc_id).addClass('show-desc');
    });
});
